from selenium import webdriver
import unittest


class GmailTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        global driver
        cls.driver = webdriver.Chrome()
        driver = cls.driver
        driver.get('https://www.google.com')

    def test_one(cls):
        print('Test One Pass')

    def test_two(cls):
        print('Test Two Pass')

    @classmethod
    def tearDownClass(cls):
        driver.quit()

if __name__ == '__main__':
    unittest.main()
