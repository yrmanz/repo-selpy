import HtmlTestRunner
import unittest
from unittest import TestLoader, TestSuite, TextTestRunner
import os
import BBCTest
from   BBCTest import BBCLogin
from   Google_Test import GmailTest
from   Gmail_Test import GmailLogin
dir = os.getcwd()



""""
Step 1.Create and load Test using TestLoader ().loadTestsFromTestCase (class).
"""
Test1=unittest. TestLoader().loadTestsFromTestCase(BBCLogin)
Test2=unittest. TestLoader().loadTestsFromTestCase(GmailTest)
Test3=unittest. TestLoader().loadTestsFromTestCase(GmailLogin)

"""
Step2.Create Test Suite using TestSuite ().
"""
TS = unittest.TestSuite ([Test1,Test2,Test3])

"""
Step 3. Run the tests using HTMLTestRunner Directory where HTML file needs to be generated
"""

dir = os.getcwd()
# create a file ex: report.html and chmod 777 (rwx – permission set)
reportfile = open(dir+"\\report.html", "w+")  # in case of append privileges set a+ instead of w+
runner=HtmlTestRunner.HTMLTestRunner(stream=reportfile)
runner.run(TS)
