from selenium import  webdriver
import unittest

class GmailLogin(unittest.TestCase):
    """
     1.Invoke browser 2.enter the url 3.Maximize window.
    """

    @classmethod
    def setUpClass(cls):
        global driver
        cls.driver = webdriver.Firefox()
        driver = cls.driver
        driver.get('https://www.gmail.com')
        driver.maximize_window()

    def test1(cls):
        print('test 1 pass')

    def test2(cls):
        print('test2 pass')

    @classmethod
    def tearDownClass(cls):
        driver.quit()

if __name__ == '__main__':
    unittest.main()
