from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
# from Constants import *
import re
import pdb


class operation():
    # obatain the element page object
    '''Contains all the GUI operations and results which obtained by performing the GUI operations'''
    def __init__(cls):
        pass

    def InvokeBrowserAndMaximize(cls, browser=None, url=None):
        global driver
        browser = browser + '()'
        webdriverBrowser = 'webdriver.' + browser
        driver = eval(webdriverBrowser)
        driver.get(url)
        driver.maximize_window()

        try:
            frame_elm = driver.find_element_by_class_name("demo-frame")
            driver.switch_to_frame(frame_elm)
            iagree = '//span[contains(text(),"I agree")]'
            IAgree = WebDriverWait(driver, 10).until(lambda driver:driver.find_element_by_xpath(iagree))
            IAgree.click()

        except:
            pass
        return driver

    def get_data(filename):
        # create an empty list to store rows
        rows = []
        # open the CSV file
        data_file = open(filename, "r")
        # create a CSV Reader from CSV file
        import csv
        reader = csv.reader(data_file)
        # skip the headers
        next(reader, None)
        # add rows from reader to list
        for row in reader:
            rows.append(tuple(row))
        return tuple(rows)

    def elePageObj(cls,filename):
        print("filename", filename)
        '''
        elePageObj class will return the pageElementObject of GUI on which GUI operations needs to be performed
        ex:
        pageObjType is type of the PageElementObject : Xpath or Css or id or name or tagName etc.
        '''
        import csv
        ElementObj = {}
        fo = open(filename, "r")  # fo is the file object for the file reader
        reader = csv.reader(fo)  # Reader creates the object containing the data of the csv file
        for row in reader:
            pageElementObjects = ['xpath', 'id', 'name', 'css', 'link_text', 'partial_link_text', 'partial_class_name',
                                  'tag_name']
            for x in pageElementObjects:
                regExp = re.compile(row[1])
                regExp = re.match(regExp, x)

                if regExp is not None:
                    regExpString = regExp.group()
                    a = 'driver.find_element_by_'
                    b = regExpString
                    c = a + b
                    value = c + '(' + '"' + row[2] + '"' + ')'
                    pageElementObject = 'WebDriverWait(driver, 10).until(lambda driver:' + value + ')'
                    ElementObj[row[0]] = pageElementObject
        return ElementObj

    def filePath(cls, file):
        from os import environ
        path = environ['TafToolset']
        path = path + '\\' + file
        return path

    def send_keys(cls, parm):
        print
        "send keys"
        cls.parm = parm
        evalPageElementObject.clear()
        evalPageElementObject.send_keys(parm)

    def click(cls):
        print
        "evalPageElementObj:", evalPageElementObject
        evalPageElementObject.click()

    # Action_Chains - hovering over the menu object an select:
    def Action_Chains(cls, menu, submenu1=None, submenu2=None, submenu3=None, submenu4=None):
        global actions
        actions = ActionChains(driver)
        actions.move_to_element(menu)
        actions.click(submenu1)
        actions.perform()

    def rightClick(cls):
        actions = ActionChains(driver)
        act = actions.context_click(evalPageElementObject)
        act.perform()

    def getAtttribute(cls, pageElementObject, name):
        value = evalPageElementObject.get_attribute(name)
        print(value)

    def DoubleClick(cls):

        # Double-clicks an element.
        # on_element: The element to double-click. If None, clicks on current mouse position.
        actions = ActionChains(driver)
        act = actions.context_click(evalPageElementObject)
        act.perform()

    def drag_and_drop(cls, source=None, target=None):
        #  Holds down the left mouse button on the source element, then moves to the target element and releases
        #  the mouse button.Args
        #  source: The element to mouse down.
        #  target: The element to mouse up.
        actions = ActionChains(driver)
        act = actions.drag_and_drop(source, target)
        act.perform()

    # Moving between windows and frames
    def switch_to_window(cls,
                         winNumber=0):  # when all the window handles are captured as list;the number to which the driver should be switched is provided in num
        cls.winNumber = winNumber
        allWinHandle = driver.window_handles
        mainWinHandle = allWinHandle[winNumber]
        print
        "allWindowhandle:", mainWinHandle
        if handle != mainWinHandle:
            driver.switch_to_window(handle)

    def switch_to_frame(cls, frameName):
        driver.switch_to_frame(frameName)

    # Handling pop Up's
    def switch_to_alert(cls, action):
        act = driver.switch_to.alert
        print
        "act", act
        try:
            if action == 'accept':
                print
                "in Accept"
                act.accept()
            elif action == 'dismiss':
                print
                "in Dismiss"
                act.dismiss()

            else:
                print
                "need to know more"
        except:
            print
            "Error"

    def select(cls, selectBy, index=None, text=None, value=None):
        from selenium.webdriver.support.ui import Select
        select = Select(evalPageElementObject)
        try:
            if selectBy == index:
                select.select_by_index(index)
            elif selectBy == visible_text:
                select.select_by_visible_text("text")
            elif selectBy == value:
                select.select_by_value(value)
            else:
                print
                "Inappropriate parameter passed"

        except AttributeNotPassed:
            print
            "AttributeNotPassed"


if __name__ == "__main__":
    inst = operation()
    readers1 = inst.readers('ddtLogin.csv')
    print (readers1)
    #filepath = inst.filePath("eleObjHome.csv")
    #print('filepath', filepath)
    #filePath1 = 'r' + '\''+filepath+'\''
    #print('filepath1',filePath1)
    #filePath1=eval(filePath1)

    #invoke = inst.InvokeBrowserAndMaximize('Firefox', 'https://www.google.com')
    # man      = inst.readers(filePath)
    # print man
    # filePath = inst.filePath("objectRepository_Login.csv")
    # casio = inst.elePageObj()
    #casio = inst.elePageObj('eleObjHome.csv')
    #print("casio", casio)