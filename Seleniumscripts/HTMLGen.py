import HtmlTestRunner
import unittest
import os
import BBCTest
from BBCTest import BBCLogin
from Google_Test import GmailTest
from Gmail_Test import GmailLogin
dir = os.getcwd()

'''
create test suite 
1) create test
2) create test suite 
3) Run HTML test Runner 
'''

# Create test
class CreateTestSuite():
    """
    x is how many tests needs to be created i.e equivalent number 'class' we need to execute
    """
    def testsuite(cls,*x):
        global a
        a = []
        for i in x:
            test = 'test' + i
            print(test)
            a.append(test)
        return a

    def evaltest(cls,*classname):
        global b
        b = []
        for k in classname:
            str1 = 'unittest.TestLoader().loadTestsFromTestCase'+'('+k+')'
            str1 = eval(str1)
            b.append(str1)
        return b

    def gensuite(cls):
        global strlist
        print('a', a)
        print('b', b)
        lengthofstr = len(a)
        print('los', lengthofstr)
        strlist = {}
        for item in range(0, lengthofstr):
            strlist[a[item]]=b[item]
        return strlist

    def htmlreport(cls):
        global strlist
        strfinals = []
        for x,y in strlist.items():
            x=y
            strfinals.append(x)
        print('strfinals', strfinals)
        TS = unittest.TestSuite(strfinals)
        dir = os.getcwd()
        # create a file ex: report.html and chmod 777 (rwx – permission set)
        reportfile = open(dir+"\\report.html", "w+")  # in case of append privileges set a+ instead of w+
        runner = HtmlTestRunner.HTMLTestRunner(stream=reportfile)
        runner.run(TS)

if __name__ == '__main__':
    a=('One','Two','Three')
    b=('BBCLogin','GmailTest','GmailLogin')
    inst=CreateTestSuite()
    inst.testsuite(*a)
    inst.evaltest(*b)
    gen = inst.gensuite()
    print('gen', gen)
    htmlreport = inst.htmlreport()
    #inst.htmlreport()










