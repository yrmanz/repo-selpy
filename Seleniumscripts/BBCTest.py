from selenium import  webdriver
from selenium.webdriver.support.wait import  WebDriverWait
from selenium.webdriver.support.select import  Select
from selenium.webdriver.support import  expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
import unittest
import HtmlTestRunner
from browserandurl import Cons
from guioperations import operation
from ddt import data, unpack, ddt
import time
ist = operation.get_data('ddtLogin.csv')


def get_data(filename):
    # create an empty list to store rows
    rows = []
    # open the CSV file
    data_file = open(filename, "r")
    # create a CSV Reader from CSV file
    import csv
    reader = csv.reader(data_file)
    # skip the headers
    next(reader, None)
    # add rows from reader to list
    for row in reader:
        rows.append(tuple(row))
    return tuple(rows)

#a = get_data('ddtLogin.csv')
#print(a)


@ddt
class BBCLogin(unittest.TestCase, Cons, operation):
    """
     1.Invoke browser 2.enter the url 3.Maximize window.
    """

    @classmethod
    def setUpClass(cls):
        global driver
        a = Cons('bbc', 'Chrome')
        b = a.funcConstants()
        driver = super().InvokeBrowserAndMaximize(0, b[0], b[1])

    def test1(cls):
        """
        Test the GUI Standards
        """
        eleObjList = super().elePageObj('eleObjMain.csv')
        signInObj = eval(eleObjList['signIn'])
        try:
            print(signInObj.is_enabled())
            print(signInObj.is_displayed())
            print(signInObj.size)
        except:
            print('Defined Error')
        driver.refresh()
        signInObj.click()
        try:
            page_title = driver.title
            cls.assertEqual(page_title, 'BBC - Homepage')
        except AssertionError:
            print ('Assertion Error')


    def test2(cls):
        """
        Register and Close
        """
        eleObjDicTest2 = super().elePageObj('eleObjRegister.csv')
        print(eleObjDicTest2)
        registerButtonObj = eval(eleObjDicTest2['registerButton'])
        registerButtonObj.click()
        time.sleep(5)
        # pdb.set_trace()
        orOverObj = eval(eleObjDicTest2['orOver'])
        orOverObj.click()
        dobDateObj = eval(eleObjDicTest2['dobDate'])
        dobDateObj.send_keys('25')
        dobMonthObj = eval(eleObjDicTest2['dobMonth'])
        dobMonthObj.send_keys('11')
        dobYearObj = eval(eleObjDicTest2['dobYear'])
        dobYearObj.send_keys('1980')
        continueObj = eval(eleObjDicTest2['continue'])
        continueObj.click()
        userNameObj = eval(eleObjDicTest2['userName'])
        userNameObj.send_keys('yrmanz@hotmail.com')
        passWordObj = eval(eleObjDicTest2['passWord'])
        passWordObj.send_keys('Jesus_123')
        regpageCloseObj = eval(eleObjDicTest2['regpageClose'])
        regpageCloseObj.click()

    @data(*get_data('ddtLogin.csv'))
    @unpack
    def test_three(cls,userName,password):
        """
        Test for login - for any changes for usrName and password update the ddtLogin.csv
        """
        print('in test3')
        eleObjDicTest3 = super().elePageObj('eleObjHome.csv')
        signInObj = eval(eleObjDicTest3['signIn'])
        signInObj.click()
        input_emailObj = eval(eleObjDicTest3['input_email'])
        input_emailObj.clear()
        input_emailObj.send_keys(userName)
        input_passwordObj = eval(eleObjDicTest3['input_password'])
        input_passwordObj.clear()
        input_passwordObj.send_keys(password)
        signInSubmitObj = eval(eleObjDicTest3['signInSubmit'])
        signInSubmitObj.click()
        try:
            wrongPasswordObj = eval(eleObjDicTest3['wrongPassword'])
            #acceptOkObj = eval(eleObjDicTest3['wrongPassword'])
            str = wrongPasswordObj.text()
            print(str)
        except:
            print('wrong GUI behaviour')

    @classmethod
    def tearDownClass(cls):
        driver.quit()

if __name__ == '__main__':
    reportfile = open(dir+"\\report.html", "w+")  # in case of append privileges set a+ instead of w+
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output=reportfile))